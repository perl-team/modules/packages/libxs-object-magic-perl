libxs-object-magic-perl (0.05-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ gregor herrmann ]
  * Remove generated file via debian/clean. (Closes: #1048556)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Enable all hardening flags.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Mar 2024 11:21:41 +0100

libxs-object-magic-perl (0.05-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 04:25:57 +0100

libxs-object-magic-perl (0.05-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update Test::use::ok build dependency.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian spelling-error-in-description warning

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  * Import upstream version 0.05
  * Refresh debian/copyright.
    New upstream contact, removed files, bumped years.
  * Update debian/upstream/metadata.
  * Install new CONTRIBUTING file.
  * Update build dependencies.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 4.5.0.
  * Set Rules-Requires-Root: no.
  * Drop unneeded version constraints from (build) dependencies.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Fri, 06 Mar 2020 20:43:25 +0100

libxs-object-magic-perl (0.4-1) unstable; urgency=low

  * Initial release (closes: #696101).

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Dec 2012 19:21:33 +0100
